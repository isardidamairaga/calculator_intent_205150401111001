package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class IntentActivity extends AppCompatActivity {

    TextView angkaKeluar;
    String process;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        angkaKeluar = findViewById(R.id.angkaKeluar);

        Bundle b1 =  getIntent().getExtras();
        String s1 = b1.getString ("angka");

        process=s1;
        process=process.replaceAll("x","*");
        process=process.replaceAll("%","/100");
        process=process.replaceAll("÷","/");
        String word= s1;
        int input = word.length();

        org.mozilla.javascript.Context rhino = Context.enter();

        rhino.setOptimizationLevel(-1);

        String finalResult = "";

        try {
            Scriptable scriptable = rhino.initSafeStandardObjects();
            finalResult = rhino.evaluateString (scriptable,process,"javascript",1,null).toString();
        }catch (Exception e){
            finalResult="0";
        }
        if(input!=0) {
            angkaKeluar.setText(s1+"\n="+finalResult);
        }else{
            angkaKeluar.setText("");
        }

    }
}